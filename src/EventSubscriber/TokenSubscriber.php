<?php
// src/EventSubscriber/TokenSubscriber.php
namespace App\EventSubscriber;

use App\Repository\Auth\AuthRepository;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TokenSubscriber implements EventSubscriberInterface {
    protected $authRepository;

    function __construct(AuthRepository $authRepository) {
        $this->authRepository = $authRepository;
    }

    private function isAuthNeeded (ControllerEvent $event) {
        $controller = is_array($event->getController()) ? $event->getController()[0] : $event->getController();
        return ($controller instanceof TokenAuthenticatedController) ? $controller : null;
    }

    public function onKernelController(ControllerEvent $event) {
        $controller = $this->isAuthNeeded($event);
        if (!$controller) {
            return true;
        }

        $authorization = $event->getRequest()->headers->get('Authorization');
        if (!$authorization) {
            throw new AccessDeniedHttpException('To perform this action you need to be auth! 1');
        }
        $user = $this->authRepository->findOneBy(['authorization' => $authorization]);
        if (!$user) {
            throw new AccessDeniedHttpException('To perform this action you need to be auth! 2');
        }
        $controller->user = $user;
        return true;
    }

    public static function getSubscribedEvents() {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}