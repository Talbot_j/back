<?php
namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Repository\RepositoryFactory;
use Doctrine\Persistence\ManagerRegistry;

class BaseRepository extends ServiceEntityRepository
{
    private $success = true;
    private $errors = [];

    public function __construct(ManagerRegistry $registry, $entityClass = null) {
        parent::__construct($registry, $entityClass);
    }

    protected function setError($message) {
        $this->success = false;
        $this->errors[] = $message;
        return false;
    }

    public function getError() {
        if ($this->success === false) {
            return $this->errors;
        }
        return null;
    }
}
