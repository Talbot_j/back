<?php

namespace App\Repository\User;

use App\Entity\ProfilePicture;
use App\Entity\User;
use App\Entity\Contact;

use App\Repository\BaseRepository;
use App\Library\FileFactory;
use App\Repository\ProfilePicture\ProfilePictureRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;
use Twig\Profiler\Profile;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends BaseRepository
{
    // Constructors
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function getContacts(User $user) {
        $em = $this->getEntityManager();
        return $em->getRepository(Contact::class)
            ->findBy(array('user' => $user));
    }
    public function getUserKnowsYou(User $user) {
        $em = $this->getEntityManager();
        return $em->getRepository(Contact::class)
            ->findBy(array('contact' => $user));
    }

    public function setContacts(User $user, $phoneList) {
        $em = $this->getEntityManager();
        for ($i = count($phoneList) - 1; $i >= 0; $i--) {
            $phoneList[$i] = str_replace(' ', '', $phoneList[$i]);
        }
        $userList = $em->getRepository(User::class)
            ->findBy(array('phone' => $phoneList));
        $contacts = $this->getContacts($user);
        foreach ($userList as $ul) {
            $exist = 0;
            foreach ($contacts as $contact) {
                if ($contact->getContact() === $ul) {
                    $exist = 1;
                    break;
                }
            }
            if (!$exist) {
                $contacts[] = (new Contact())->setUser($user)
                    ->setContact($ul);
            }
        }
        $this->saveUnique($contacts);
    }

    private function saveUnique(Array $users) {
        try {
            $em = $this->getEntityManager();
            foreach ($users as $user) {
                $em->persist($user);
            }
            $em->flush();
            return true;
        } catch (ORMException $e) {
        }
        return false;
    }

}
