<?php

namespace App\Repository\User;

use App\Entity\ProfilePicture;
use App\Entity\User;

use App\Repository\BaseRepository;
use App\Library\FileFactory;
use App\Repository\ProfilePicture\ProfilePictureRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;
use Twig\Profiler\Profile;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends BaseRepository
{
    // Constructors
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function update(User $user, $params) {
        if (isset($params->firstName)) {
            $user->setFirstName($params->firstName);
        }
        if (isset($params->profilePicture->data)
            && isset($params->profilePicture->mime)) {
            $fileName = FileFactory::save($params->profilePicture->data);
            $picture = (new ProfilePicture())
                ->setPath($fileName)
                ->setMime($params->profilePicture->mime);
            $user->setProfilePicture($picture);
        }
        return $this->saveUnique($user);
    }

    private function saveUnique(User $user) {
        try {
            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();
            return true;
        } catch (ORMException $e) {
        }
        return false;
    }

    public function getFromPhoneList($phoneList) {
        $contacts = [];
        foreach ($phoneList as $pn) {
            $contacts[] = $pn->contact;
        }
        return $contacts;
    }

    public function getFromOtherUserPhoneList($phoneList) {
        $contacts = [];
        foreach ($phoneList as $pn) {
            $contacts[] = $pn->user;
        }
        return $contacts;
    }

}
