<?php

namespace App\Repository\ProfilePicture;

use App\Entity\ProfilePicture;

use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\ORMException;

class ProfilePictureRepository extends BaseRepository
{
    // Constructors
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ProfilePicture::class);
    }
    public function save(ProfilePicture $profilePicture) {
        try {
            $em = $this->getEntityManager();
            $em->persist($profilePicture);
            $em->flush();
            return true;
        } catch (ORMException $e) {
        }
        return false;
    }
}
