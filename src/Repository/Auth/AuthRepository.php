<?php
namespace App\Repository\Auth;

use App\Library\SMSFactory;
use App\Library\FormatValidator;

use App\Entity\User;

use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;

/**
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 */
class AuthRepository extends BaseRepository
{
    /** @var User $user */
    private $user =          null;

    // Constructors
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }


    public function authenticate($params) {
        $this->user = new User();
        if (isset($params->phoneAuth) && isset($params->phone)) {
            $this->login($params->phone, $params->phoneAuth);
            return $this->user;
        } else if (isset($params->phone) && FormatValidator::isPhoneValid($params->phone)) {
            $this->register($params->phone);
            return null;
        } else {
            // ToDo: Manage theses errors separately
            $this->setError('Invalid phone format');
            $this->setError("Missing parameters");
        }
        return null;
    }

    private function register($phoneNumber) {
        $this->user = $this->findOneBy(['phone' => $phoneNumber]);
        if (!$this->user)
            $this->user = new User();
        $this->user->setPhone($phoneNumber);
        do {
            $auth = strval(mt_rand(100000, 999999));
            $this->user->setPhoneAuth($auth);
        } while (!$this->saveUnique());
        $smsFactory = new SMSFactory();
        return $smsFactory->sendPhoneAuth($this->user->getPhoneAuth(), $this->user->phone);
    }

    private function login($phoneNumber, $phoneAuth) {
        $this->user = $this->findOneBy(['phone' => $phoneNumber]);
        if ($this->user->getPhoneAuth() === $phoneAuth) {
            do {
                $auth = bin2hex(openssl_random_pseudo_bytes(64));
                $this->user->setAuthorization($auth);
                if ($this->user->profilePicture) {
                    $this->user->profilePicture->getData();
                }
            } while (!$this->saveUnique());
        } else {
            $this->setError("Wrong phoneAuth");
        }
        return;
    }

    // ToDo: Refactoring this function
    private function saveUnique() {
        try {
            $em = $this->getEntityManager();
            $em->persist($this->user);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            return null;
        } catch (ORMException $e) {
        }
        return false;
    }
}
