<?php

namespace App\Repository\Conversation;

use App\Entity\Conversation;
use App\Entity\Group;
use App\Entity\Group_X_Users;
use App\Entity\ProfilePicture;
use App\Entity\User;
use App\Entity\Message;

use App\Library\FileFactory;
use App\Repository\Auth\AuthRepository;
use App\Repository\BaseRepository;
use App\Repository\User\UserRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;

class GroupRepository extends BaseRepository
{
    /** @var Group $group */
    private $group;

    // Constructors
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Group::class);
    }

    public function createGroup(User $user, $params) {
        $userRepository = $this->getEntityManager()->getRepository(User::class);
        $dests = $userRepository->findBy(array('id' => $params->users));
        $dests[] = $user;
        $this->group = (new Group())
            ->setUsers($dests)
            ->setTitle($params->title);
        if (isset($params->picture->data)
            && isset($params->picture->mime)) {
            $fileName = FileFactory::save($params->picture->data);
            $picture = (new ProfilePicture())
                ->setPath($fileName)
                ->setMime($params->picture->mime);
            $this->group->setProfilePicture($picture);
        }

        try {
            $em = $this->getEntityManager();
            $em->persist($this->group);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            var_dump($e);
        } catch (ORMException $e) {
            var_dump($e);
            // ToDo: Log error
        }
        return $this;
    }
    public function findByUserIn(User $user) {
        $group_in = $this->getEntityManager()->getRepository(Group_X_Users::class)->findBy(['user' => $user]);
        $group_in_id = array_map(function($c) {
            return $c->group->getId();
        }, $group_in );
        $groups = $this->getEntityManager()->getRepository(Group::class)->findBy(['id' => $group_in_id]);
        array_map(function(Group $g) {
            if ($g->picture) {
                $g->picture->getData();
            }
            return $g->setUsersAsIds();
        }, $groups );
        return $groups;
    }

    private function aggregateGroupsX($groupXArray, $sameUser, $result = []) {
        if (!$groupXArray) {
            return $result;
        }
        $newArray = $groupXArray;
        $tmp = array_splice($newArray, 0, 1);

        $i = 0;
        while (isset($newArray[$i])) {
            if ($newArray[$i]->group->id === $tmp[0]->group->id) {
                $tmp[] = $newArray[$i];
                array_splice($newArray, $i, 1);
            } else {
                $i++;
            }
        }
        if (count($tmp) === 2) {
            $isThisSameUser = $tmp[0]->user->id === $tmp[1]->user->id;
            if ($sameUser === $isThisSameUser) {
                $result[] = $tmp[0]->group->id;
            }
        }
        return $this->aggregateGroupsX($newArray, $sameUser, $result);
    }

    public function twoUsersIn(User $user, User $user2) {
        $groups_in = $this->getEntityManager()->getRepository(Group_X_Users::class)->findBy(['user' => [$user, $user2]]);
        $groups_in = $this->aggregateGroupsX($groups_in, $user === $user2);
        $groups = $this->getEntityManager()->getRepository(Group::class)->findBy(['id' => $groups_in]);
        foreach ($groups as $g) {
            if (!$g->title) {
                return $g;
            }
        }
        return null;
    }
}
