<?php

namespace App\Repository\Conversation;

use App\Entity\Conversation;
use App\Entity\Group;
use App\Entity\Group_X_Users;
use App\Entity\Photo;
use App\Entity\User;
use App\Entity\Message;

use App\Library\FileFactory;
use App\Repository\Auth\AuthRepository;
use App\Repository\BaseRepository;
use App\Repository\User\UserRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends BaseRepository
{
    /** @var Message $message */
    private $message;

    // Constructors
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Message::class);
    }

    public function findByUserIn(User $user)
    {
        $group_in = $this->getEntityManager()->getRepository(Group_X_Users::class)->findBy(['user' => $user]);
        $group_in_id = array_map(function($c) {
            return $c->group->getId();
        }, $group_in );

        $conversations = $this->getEntityManager()->getRepository(Conversation::class)->findBy(['group' => $group_in_id]);
        $convs_in_id = array_map(function(Conversation $c) {
            return $c->getId();
        }, $conversations );
        $messages = $this->findBy(['conversation' => $convs_in_id]);
        foreach ($messages as $m) {
            $m->getPhotos();
        }
        return $messages;
    }

    public function findByConversation($conversationId)
    {
        $messages = $this->findBy(['conversation' => $conversationId]);
        var_dump($messages[0]->getConversation()->getLatitude());
        return $messages;
    }

    public function createMessage(User $user, $params)
    {
        $conversation = $this->getEntityManager()->getRepository(Conversation::class)
            ->findOneBy(array('id' => $params->conversation));

        $message = (new Message())
            ->setCreatedBy($user)
            ->setConversation($conversation)
            ->setMessage($params->message);
        try {
            $em = $this->getEntityManager();
            $em->persist($message);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            var_dump($e);
        } catch (ORMException $e) {
            var_dump($e);
            // ToDo: Log error
        }
        return $this;
    }

    public function uploadPhoto(User $user, $params)
    {
        $message = $this->findOneBy(array('id' => $params->message));
        if (!isset($message)) {
            $this->setError("Message not found");
            return $this;
        }
        if (!isset($params->photo->data)
            || !isset($params->photo->mime)) {
            $this->setError("Photo not found");
            return $this;
        }
        $fileName = FileFactory::save($params->photo->data);
        $photo = (new Photo())
            ->setPath($fileName)
            ->setMime($params->photo->mime);

        $message->addPhoto($photo);
        try {
            $em = $this->getEntityManager();
            $em->persist($message);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            var_dump($e);
        } catch (ORMException $e) {
            var_dump($e);
            // ToDo: Log error
        }
        return $this;
    }


}
