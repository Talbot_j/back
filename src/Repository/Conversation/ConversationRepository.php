<?php

namespace App\Repository\Conversation;

use App\Entity\Conversation;
use App\Entity\Group;
use App\Entity\Group_X_Users;
use App\Entity\LogOpen;
use App\Entity\Photo;
use App\Entity\User;
use App\Entity\Message;

use App\Library\FileFactory;
use App\Repository\Auth\AuthRepository;
use App\Repository\BaseRepository;
use App\Repository\User\UserRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\ORMException;

/**
 * @method Conversation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversation[]    findAll()
 * @method Conversation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationRepository extends BaseRepository
{
    /** @var Conversation $conversation */
    private $conversation;

    // Constructors
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Conversation::class);
    }

    public function createOneConversation($user, $group, $content, $position) {
        $conversation = (new Conversation())
            ->setCreatedBy($user)
            ->setGroup($group)
            ->setPosition($position);
        $path = FileFactory::save($content);
        $photo = (new Photo())
            ->setMime('jpg')
            ->setPath($path);
        $message = (new Message())
            ->setCreatedBy($user)
            ->setConversation($conversation)
            ->setMessage('')
            ->addPhoto($photo);
        try {
            $em = $this->getEntityManager();
            $em->persist($message);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            var_dump($e);
        } catch (ORMException $e) {
            var_dump($e);
        }
        return false;
    }

    public function createConversation(User $user, $params) {
        $userRepository = $this->getEntityManager()->getRepository(User::class);
        $groupRepository = $this->getEntityManager()->getRepository(Group::class);
        $groups = [];
        if ($params->dests->groups) {
            $groups = $groupRepository->findBy(array('id' => $params->dests->groups));
        }
        if ($params->dests->users) {
            foreach ($params->dests->users as $u) {
                $dest_user = $userRepository->findOneBy(array('id' => $u));
                $dest_grp = $groupRepository->twoUsersIn($user, $dest_user);
                if (!$dest_grp) {
                    $dest_grp = (new Group())->setUsers([$user, $dest_user]);
                }
                $groups[] = $dest_grp;
            }
        }

        foreach ($groups as $g) {
            $this->createOneConversation($user, $g, $params->message, $params->position);
        }
        return $this;
    }

    public function findByUserIn(User $user)
    {
        $group_in = $this->getEntityManager()->getRepository(Group_X_Users::class)->findBy(['user' => $user]);
        $group_in_id = array_map(function($c) {
            return $c->group->getId();
        }, $group_in );
        $conv_in =  $this->findBy(['group' => $group_in_id]);
        $conv_in_id = array_map(function($c) {
            return $c->getId();
        }, $conv_in );
        $message_in =  $this->getEntityManager()->getRepository(Message::class)->findBy(['conversation' => $conv_in_id]);
        $tot = count($message_in);
        $tmp = [];
        for ($n = 0; $n < $tot; $n++)
        {
            $id = $message_in[$n]->conversation->id;
            if ((!isset($tmp[$id])) || ($tmp[$id]->created_at->getTimestamp() < $message_in[$n]->created_at->getTimestamp()))
            {
                $tmp[$id] = $message_in[$n];
            }
        }
        for ($n = 0; $n < count($conv_in); $n++)
        {
            $conv_id = $conv_in[$n]->getId();
            if (isset($tmp[$conv_id]))
            {
                $conv_in[$n]->setLastMessage($tmp[$conv_id]);
            }
        }
        return $conv_in;
    }

    public function unlockWax(User $user, $params) {
        $conversation = $this->getEntityManager()->getRepository(Conversation::class)
            ->findOneBy(['id' => $params->conversation]);
        $lo = (new LogOpen())->setUser($user)
            ->setConversation($conversation)
            ->setOpened();
        try {
            $em = $this->getEntityManager();
            $em->persist($lo);
            $em->flush();
            return true;
        } catch (UniqueConstraintViolationException $e) {
            var_dump($e);
        } catch (ORMException $e) {
            var_dump($e);
        }
        return false;
    }

    public function getUnlockedWaxList(User $user) {
        $lo = $this->getEntityManager()->getRepository(LogOpen::class)
            ->findBy(['user' => $user]);
        return array_map(function($c) {
            return $c->conversation->getId();
        }, $lo );

    }
}
