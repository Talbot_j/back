<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BaseController extends AbstractController {
    /** @var User $user */
    public $user;

    protected function error($errors = 'Unknown Error', $statusCode = 400) {
        return new JsonResponse([
            'success' => false,
            'errors' => $errors
        ], $statusCode);
    }

    protected function success($response = 'Unknown Response', $statusCode = 200) {
        return new JsonResponse([
            'success' => true,
            'response' => $response
        ], $statusCode);
    }
}