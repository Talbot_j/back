<?php
namespace App\Controller\Auth;

use App\Controller\BaseController;
use App\Repository\Auth\AuthRepository;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends BaseController {
    /**
     * @Route("/login", name="login", methods={"POST", "GET"})
     *
     * @param Request $request
     * @param AuthRepository $authRepository
     * @return JsonResponse
     */
    public function login(Request $request, AuthRepository $authRepository) {
        $content = json_decode($request->getContent());
        $res = $authRepository->authenticate($content);
        $errors = $authRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        if ($res) {
            return $this->success([
                'authorization' => $res->getAuthorization(),
                'user' => $res
            ], 200);
        }
        return $this->success(null, 200);
    }
}