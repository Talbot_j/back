<?php
// src/Controller/UserController.php
namespace App\Controller\Conversation;

use App\Controller\BaseController;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use App\Repository\Conversation\GroupRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GroupController extends BaseController implements TokenAuthenticatedController {
    /**
     * @Route("/group", methods={"GET"})
     *
     * @param Request $request
     * @param GroupRepository $groupRepository
     * @return JsonResponse
     */
    public function getGroup(Request $request, GroupRepository $groupRepository)
    {
        $res = $groupRepository->findByUserIn($this->user);
        return $this->success($res);
    }

    /**
     * @Route("/group", methods={"POST"})
     *
     * @param Request $request
     * @param GroupRepository $groupRepository
     * @return JsonResponse
     */
    public function postGroup(Request $request, GroupRepository $groupRepository)
    {
        $res = $groupRepository->createGroup($this->user, json_decode($request->getContent()));
        $errors = $groupRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }
}
