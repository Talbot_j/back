<?php
// src/Controller/UserController.php
namespace App\Controller\Conversation;

use App\Controller\BaseController;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use App\Repository\Conversation\MessageRepository;
use App\Repository\Media\PhotoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends BaseController implements TokenAuthenticatedController {
    /**
     * @Route("/message", methods={"GET"})
     *
     * @param Request $request
     * @param MessageRepository $messageRepository
     * @return JsonResponse
     */
    public function getMessage(Request $request, MessageRepository $messageRepository)
    {
        $res = $messageRepository->findByUserIn($this->user);
        return $this->success($res);
    }

    /**
     * @Route("/message/{token}", requirements={"token"=".+"}, methods={"GET"})
     *
     * @param $token
     * @param Request $request
     * @param MessageRepository $messageRepository
     * @return JsonResponse
     */
    public function getMessageInConversation($token, Request $request, MessageRepository $messageRepository)
    {
        $res = $messageRepository->findByConversation($token);

        return $this->success($res);
    }

    /**
     * @Route("/message", methods={"POST"})
     *
     * @param Request $request
     * @param MessageRepository $messageRepository
     * @return JsonResponse
     */
    public function postMessage(Request $request, MessageRepository $messageRepository)
    {
        $res = $messageRepository->createMessage($this->user, json_decode($request->getContent()));
        $errors = $messageRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }

    /**
     * @Route("/photo", methods={"POST"})
     *
     * @param Request $request
     * @param MessageRepository $messageRepository
     * @return JsonResponse
     */
    public function postPhoto(Request $request, MessageRepository $messageRepository)
    {
        $res = $messageRepository->uploadPhoto($this->user, json_decode($request->getContent()));
        $errors = $messageRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }

    /**
     * @Route("/photo/{token}", requirements={"token"=".+"}, methods={"GET"})
     *
     * @param $token
     * @param Request $request
     * @param PhotoRepository $photoRepository
     * @return JsonResponse
     */
    public function getPhoto($token, Request $request, PhotoRepository $photoRepository)
    {
        $res = $photoRepository->findOneById($token);






        return $this->success($res);

    }
}
