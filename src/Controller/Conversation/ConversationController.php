<?php
// src/Controller/UserController.php
namespace App\Controller\Conversation;

use App\Controller\BaseController;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use App\Repository\Conversation\ConversationRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ConversationController extends BaseController implements TokenAuthenticatedController {
    /**
     * @Route("/unlock", methods={"POST"})
     *
     * @param Request $request
     * @param ConversationRepository $conversationRepository
     * @return JsonResponse
     */
    public function unlockWax(Request $request, ConversationRepository $conversationRepository)
    {
        $res = $conversationRepository->unlockWax($this->user, json_decode($request->getContent()));
        $errors = $conversationRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }

    /**
     * @Route("/unlock", methods={"GET"})
     *
     * @param Request $request
     * @param ConversationRepository $conversationRepository
     * @return JsonResponse
     */
    public function getUnlockedWax(Request $request, ConversationRepository $conversationRepository)
    {
        $res = $conversationRepository->getUnlockedWaxList($this->user);
        return $this->success($res);
    }

    /**
     * @Route("/conversation", methods={"GET"})
     *
     * @param Request $request
     * @param ConversationRepository $conversationRepository
     * @return JsonResponse
     */
    public function getConversation(Request $request, ConversationRepository $conversationRepository)
    {
        $res = $conversationRepository->findByUserIn($this->user);
        return $this->success($res);
    }

    /**
     * @Route("/conversation", methods={"POST"})
     *
     * @param Request $request
     * @param ConversationRepository $conversationRepository
     * @return JsonResponse
     */
    public function postConversation(Request $request, ConversationRepository $conversationRepository)
    {
        $res = $conversationRepository->createConversation($this->user, json_decode($request->getContent()));
        $errors = $conversationRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }
}
