<?php
// src/Controller/UserController.php
namespace App\Controller\User;

use App\Controller\BaseController;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use App\Repository\User\ContactRepository;
use App\Repository\User\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseController implements TokenAuthenticatedController {
    /**
     * @Route("/user", methods={"POST"})
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function postUserInfo(Request $request, UserRepository $userRepository)
    {
        $content = json_decode($request->getContent());
        $res = $userRepository->update($this->user, $content);
        $errors = $userRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        if ($this->user->profilePicture)
            $this->user->profilePicture->getData();
        return $this->success($this->user);
    }


    /**
     * @Route("/contact", methods={"GET"})
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param ContactRepository $contactRepository
     * @return JsonResponse
     */
    public function getContactsInfo(Request $request, UserRepository $userRepository, ContactRepository $contactRepository) {
        $content = $contactRepository->getContacts($this->user);
        $res = $userRepository->getFromPhoneList($content);

        $content = $contactRepository->getUserKnowsYou($this->user);
        $res = array_merge($res, $userRepository->getFromOtherUserPhoneList($content));

        $errors = $userRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        $doublon = [];
        $return = [];
        foreach ($res as $r) {
            if (!in_array($r->id, $doublon)) {
                $doublon[] = $r->id;
                if ($r->profilePicture) {
                    $r->profilePicture->getData();
                }
                $return[] = $r;
            }
        }
        return $this->success($return);
    }

}

