<?php
// src/Controller/UserController.php
namespace App\Controller\User;

use App\Controller\BaseController;
use App\Controller\MiddleInterface\TokenAuthenticatedController;
use App\Repository\User\ContactRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends BaseController implements TokenAuthenticatedController {
    /**
     * @Route("/contact", methods={"POST"})
     *
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @return JsonResponse
     */
    public function postContactsInfo(Request $request, ContactRepository $contactRepository) {
        $content = json_decode($request->getContent());
        $res = $contactRepository->setContacts($this->user, $content->phoneNumbers);
        $errors = $contactRepository->getError();
        if ($errors) {
            return $this->error($errors);
        }
        return $this->success($res);
    }
}
