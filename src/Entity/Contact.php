<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User\ContactRepository")
 * @Table(name="wax_contact")
 */
class Contact
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @Column(type="datetimetz")
     */
    public $created_at;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     */
    public $user;

    /**
     * @ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     */
    public $contact;

    public function __construct() {
        $this->created_at = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getContact(): ?User {
        return $this->contact;
    }

    public function setContact(User $contact)
    {
        $this->contact = $contact;
        return $this;
    }

}
