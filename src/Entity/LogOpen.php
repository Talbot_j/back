<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 */
class LogOpen
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @ManyToOne(targetEntity="User", cascade={"persist"})
     */
    public $user;

    /**
     * @ManyToOne(targetEntity="Conversation", cascade={"persist"})
     */
    public $conversation;

    /**
     * @Column(type="datetimetz", nullable=true)
     */
    public $opened;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user) : self {
        $this->user = $user;
        return $this;
    }

    public function getConversation(): ?Conversation {
        return $this->conversation;
    }

    public function setConversation(Conversation $conversation) : self {
        $this->conversation = $conversation;
        return $this;
    }

    public function __construct() {
    }

    public function setOpened() : self {
        $this->opened = new DateTime();
        return $this;
    }

    public function getOpened() {
        return $this->opened;
    }
}
