<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Twig\Profiler\Profile;

/**
 * @ORM\Entity()
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=128, nullable=true, unique=true)
     */
    private $authorization;

    /**
     * @ORM\Column(type="string", length=128, nullable=false, options={"default" : ""})
     */
    public $firstName = "";

    /**
     * @ORM\Column(type="string", length=128, nullable=false, options={"default" : ""})
     */
    public $lastName = "";

    /**
     * @ORM\Column(type="string", length=16, nullable=true, unique=true)
     */
    public $phone;

    /**
     * @ORM\Column(type="string", length=9, nullable=true)
     */
    private $phoneAuth;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $created_at;

    /**
     * @ORM\OneToOne(targetEntity="ProfilePicture", inversedBy="user", cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumn(name="profile_picture_id", referencedColumnName="id", nullable=true)
     */
    public $profilePicture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthorization(): ?string
    {
        return $this->authorization;
    }

    public function setAuthorization(?string $authorization): self
    {
        $this->authorization = $authorization;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getProfilePicture(): ?ProfilePicture
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?ProfilePicture $profilePicture): self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getPhoneAuth(): ?string
    {
        return $this->phoneAuth;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
    public function setPhoneAuth(?string $phoneAuth): self
    {
        $this->phoneAuth = $phoneAuth;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function __construct()
    {
        $this->created_at = new DateTime();
    }
}
