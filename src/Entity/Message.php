<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 */
class Message
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @Column(type="datetimetz")
     */
    public $created_at;

    /**
     * @ManyToOne(targetEntity="App\Entity\Conversation", cascade={"persist"})
     */
    public $conversation;

    /**
     * @ManyToOne(targetEntity="App\Entity\User")
     */
    public $created_by;

    /**
     * @Column(type="string")
     */
    public $message;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Photo", cascade={"persist", "remove"}, mappedBy="message")
     */
    private $photos;
    public $photos_id = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setConversation(Conversation $conversation) : self {
        $this->conversation = $conversation;
        return $this;
    }

    public function getConversation() : Conversation {
        return $this->conversation;
    }

    public function setCreatedBy(User $user) : self {
        $this->created_by = $user;
        return $this;
    }

    public function getCreatedBy() : User {
        return $this->created_by;
    }

    public function setMessage(String $message) : self {
        $this->message = $message;
        return $this;
    }

    public function getMessage() : String {
        return $this->message;
    }

    public function getPhotos()
    {
        $photos = array();
        foreach ($this->photos as $p)
        {
            $photos[] = $p->getId();
        }
        $this->photos_id= $photos;
        return $this->photos_id;
    }

    public function addPhoto(Photo $photo)
    {
        $this->photos->add($photo);
        $photo->setMessage($this);
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTime $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function __construct()
    {
        $this->created_at = new DateTime();
        $this->photos = new ArrayCollection();
    }

}
