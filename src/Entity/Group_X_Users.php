<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 */
class Group_X_Users
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @Column(type="datetimetz")
     */
    public $created_at;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="groups", cascade={"persist"})
     */
    public $user;

    /**
     * @ManyToOne(targetEntity="Group", inversedBy="users", cascade={"persist"})
     */
    public $group;


    public function __construct() {
        $this->created_at = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(User $user) : self {
        $this->user = $user;
        return $this;
    }

    public function getGroup(): ?Group {
        return $this->group;
    }

    public function setGroup(Group $group) : self {
        $this->group = $group;
        return $this;
    }
}
