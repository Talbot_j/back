<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Conversation\GroupRepository")
 * @Table(name="wax_group")
 */
class Group
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @Column(type="datetimetz")
     */
    public $created_at;

    /**
     * @Column(type="string", nullable=true)
     */
    public $title;

    /**
     * @OneToMany(targetEntity="Group_X_Users", mappedBy="group", cascade={"persist"})
     */
    public $users;
    private $users_id = [];

    /**
     * @ORM\OneToOne(targetEntity="ProfilePicture", cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id", nullable=true)
     */
    public $picture;

    public function __construct() {
        $this->created_at = new DateTime();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Array {
        return $this->users;
    }
    public function addUser(User $user)
    {
        $user_lnk = (new Group_X_Users())
            ->setUser($user)
            ->setGroup($this);
        if (!$this->users->contains($user_lnk)) {
            $this->users->add($user_lnk);
        }
    }

    public function setTitle(string $title) : self {
        $this->title = $title;
        return $this;
    }

    public function getTitle() : string {
        return $this->title;
    }

    public function setUsers(Array $users) : self {
        for ($i = count($users) - 1; $i >= 0; $i--) {
            $this->addUser($users[$i]);
        }
        return $this;
    }
    public function setUsersAsIds() : self {
        foreach ($this->users as $user) {
            $this->users_id[] = $user->getUser()->getId();
        }
        $this->users = $this->users_id;
        return $this;
    }
    public function getProfilePicture(): ?ProfilePicture
    {
        return $this->picture;
    }

    public function setProfilePicture(?ProfilePicture $picture): self
    {
        $this->picture = $picture;

        return $this;
    }
}
