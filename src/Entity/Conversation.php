<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Conversation\ConversationRepository")
 */
class Conversation
{
    /**
     * @Id()
     * @GeneratedValue()
     * @Column(type="integer")
     */
    public $id;

    /**
     * @Column(type="datetimetz")
     */
    public $created_at;

    /**
     * @ManyToOne(targetEntity="Group", cascade={"persist"})
     */
    public $group;

    /**
     * @Column(type="float")
     */
    public $longitude;

    /**
     * @Column(type="float")
     */
    public $latitude;

    /**
     * @ManyToOne(targetEntity="User")
     */
    public $created_by;

    public $last_message;

    public $last_message_at;

    public $last_message_is_photo;

    public function setLastMessage(Message $last_message): self
    {
        $this->last_message = $last_message->getMessage();
        $this->last_message_at = $last_message->getCreatedAt();
        $this->last_message_is_photo = !!count($last_message->getPhotos());
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCreatedBy(User $user) : self {
        $this->created_by = $user;
        return $this;
    }

    public function getCreatedBy() : User {
        return $this->created_by;
    }

    public function setLongitude(Float $longitude) : self {
        $this->longitude = $longitude;
        return $this;
    }

    public function getLongitude() : Float {
        return $this->longitude;
    }
    public function setLatitude(Float $latitude) : self {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLatitude() : Float {
        return $this->latitude;
    }

    public function setPosition($position)
    {
        return $this->setLongitude($position->longitude)
            ->setLatitude($position->latitude);
    }

    public function setGroup(Group $group) : self {
        $this->group = $group;
        return $this;
    }

    public function getGroup() : Group {
        return $this->group;
    }

    public function __construct()
    {
        $this->created_at = new DateTime();
    }
}
