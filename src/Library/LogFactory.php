<?php
namespace App\Library;

use Psr\Log\LoggerInterface;

class LogFactory {
    /** @var LoggerInterface $logger */
    private $logger = null;

    public function __construct($logger) {
        $this->logger = $logger;
    }

    public function info($message, $details) {
        $this->logger->info($message, $details);
    }
    public function error($message, $details) {
        $this->logger->error($message, $details);
    }
    public function critical($message, $details)
    {
        $this->logger->critical($message, $details);
    }
}
