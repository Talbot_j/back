<?php
namespace App\Library;

use GuzzleHttp\Exception\ClientException;
use \Ovh\Api;
use Ovh\Exceptions\InvalidParameterException;

const _endpoint = 'ovh-eu';
const _applicationKey = "vjH2D3YBa89ZMt4d";
const _applicationSecret = "Tb2GKvgPfsK49t5EfAAfyOymqu5vf5nd";
const _consumer_key = "ZiZC8IaN6iObYfjzwxgQqeL41alWbpUH";
const _smsServices = "sms-tj152504-1";

class SMSFactory
{
    /** @var Api $conn */
    public $conn = null;

    private function connect()
    {
        try {
            $this->conn = new Api(_applicationKey,
                _applicationSecret,
                _endpoint,
                _consumer_key);
            return true;
        } catch (InvalidParameterException $e) {
            // ToDo: catch error
        }
        return false;
    }

    public function send($message, $receiver) {
        $this->connect();
        $content = (object) array(
            "charset"=> "UTF-8",
            "class"=> "phoneDisplay",
            "coding"=> "7bit",
            "noStopClause"=> true,
            "priority"=> "high",
            "validityPeriod"=> 15,
            "sender" => 'WAX',
            "message"=> $message,
            "receivers"=> [ $receiver ],
            "senderForResponse"=> false
        );
        try {
            $r = $this->conn->post('/sms/'. _smsServices . '/jobs', $content);
//        print_r($r);
            $r = $this->conn->get('/sms/'. _smsServices . '/jobs');
//        print_r($r);
        } catch (ClientException $e) {
            // Case disabled
            // ToDo: catch more error
        }
        return true;
    }

    public function sendPhoneAuth($code, $receiver) {
        $message = "Utilisez le code " . $code . " pour confirmer votre numéro de téléphone et commencer à utiliser Wax";
        return $this->send($message, $receiver);
    }
}