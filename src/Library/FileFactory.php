<?php
namespace App\Library;

const _path = "./upload/";
const _fileNameSize = 16;

class FileFactory
{
    static function checkFolder() {
        if (!file_exists(_path))
            mkdir(_path, 0700);
    }
    static function read($fileName) {
        self::checkFolder();
        return file_get_contents(_path.$fileName);
    }

    static function save($data) {
        self::checkFolder();
        do {
            $fileName = bin2hex(random_bytes(_fileNameSize));
        } while (file_exists(_path.$fileName));
        file_put_contents(_path.$fileName, $data);
        return $fileName;
    }
}